# K8s Edge Workloads
This repo holds the k8s yaml used to deploy example edge workloads onto vanilla k8s or k3s clusters

Note that you must create a secret for your cluster such that you nodes will be able to pull images from our private gitlab container registries. You can do so by performing the following using kubectl:
```bash
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=gitlab-ci-token --docker-password=<your node read token> --docker-email=<your email>
```